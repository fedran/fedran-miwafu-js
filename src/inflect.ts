import { splitSyllables } from "./syllables";

export function deflect(word: string): string
{
    return word
        .replace(/[áàā]/g, "a")
        .replace(/[éèē]/g, "e")
        .replace(/[íìī]/g, "i")
        .replace(/[óòō]/g, "o")
        .replace(/[úùū]/g, "u");
}

export function inflectMasculine(word: string): string
{
    // We don't modify single syllable words.
    const syllables = splitSyllables(deflect(word));

    if (syllables.length < 2)
    {
        return word;
    }

    // Change the accent as needed.
    const syllable = syllables[syllables.length - 2];
    const inflected = inflectMasculineSyllable(syllable);

    syllables[syllables.length - 2] = inflected;

    return syllables.join("");
}

export function inflectMasculineSyllable(syllable: string): string
{
    return syllable
        .replace(/a$/, "á")
        .replace(/e$/, "é")
        .replace(/i$/, "í")
        .replace(/o$/, "ó")
        .replace(/u$/, "ú");
}

export function inflectNeuter(word: string): string
{
    // We don't modify single syllable words.
    const syllables = splitSyllables(deflect(word));

    if (syllables.length < 2)
    {
        return word;
    }

    // Change the accent as needed.
    const syllable = syllables[syllables.length - 2];
    const inflected = inflectNeuterSyllable(syllable);

    syllables[syllables.length - 2] = inflected;

    return syllables.join("");
}

export function inflectNeuterSyllable(syllable: string): string
{
    return syllable
        .replace(/a$/, "ā")
        .replace(/e$/, "ē")
        .replace(/i$/, "ī")
        .replace(/o$/, "ō")
        .replace(/u$/, "ū");
}

export function inflectFeminine(word: string): string
{
    // We don't modify single syllable words.
    const syllables = splitSyllables(deflect(word));

    if (syllables.length < 2)
    {
        return word;
    }

    // Change the accent as needed.
    const syllable = syllables[syllables.length - 2];
    const inflected = inflectFeminineSyllable(syllable);

    syllables[syllables.length - 2] = inflected;

    return syllables.join("");
}

export function inflectFeminineSyllable(syllable: string): string
{
    return syllable
        .replace(/a$/, "à")
        .replace(/e$/, "è")
        .replace(/i$/, "ì")
        .replace(/o$/, "ò")
        .replace(/u$/, "ù");
}
