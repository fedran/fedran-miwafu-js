export function ipa(input: string): string
{
    // If we have whitespace, then we split and combine them together.
    if (input.match(/\s+/))
    {
        let parts = input.split(/\s+/);

        for (let i = 0; i < parts.length; i++)
        {
            parts[i] = ipa(parts[i]);
        }

        return parts.join(" ");
    }

    // The language is case-insensitive, so we make everything lowercase.
    input = input.toLowerCase();

    // Break apart the syllables.
    input = input.replace(/([aeiouáéíóúàèìòùāēīōū])/g, ".$1");
    input = input.replace(
        /([^aeiouáéíóúàèìòùāēīōū]+)\.([aeiouáéíóúàèìòùāēīōū])/g,
        ".$1$2");
    input = input.replace(/^\./, "");

    // Handle the accents on the vowels.
    input = input.replace(/([^aeioun]*[^aeiou])?á/, "ꜛ$1a");
    input = input.replace(/([^aeioun]*[^aeiou])?é/, "ꜛ$1e");
    input = input.replace(/([^aeioun]*[^aeiou])?í/, "ꜛ$1i");
    input = input.replace(/([^aeioun]*[^aeiou])?ó/, "ꜛ$1o");
    input = input.replace(/([^aeioun]*[^aeiou])?ú/, "ꜛ$1u");

    input = input.replace(/([^aeioun]*[^aeiou])?à/g, "ꜜ$1a");
    input = input.replace(/([^aeioun]*[^aeiou])?è/g, "ꜜ$1e");
    input = input.replace(/([^aeioun]*[^aeiou])?ì/g, "ꜜ$1i");
    input = input.replace(/([^aeioun]*[^aeiou])?ò/g, "ꜜ$1o");
    input = input.replace(/([^aeioun]*[^aeiou])?ù/g, "ꜜ$1u");

    input = input.replace(/ā/gi, "aː");
    input = input.replace(/ē/gi, "eː");
    input = input.replace(/ī/gi, "iː");
    input = input.replace(/ō/gi, "oː");
    input = input.replace(/ū/gi, "uː");

    input = input.replace(/ꜜ\./, ".ꜜ");
    input = input.replace(/ꜛ\./, ".ꜛ");

    // Handle the consontants.
    input = input.replace(/sh/g, "ɕ");
    input = input.replace(/ch/g, "tɕ");
    input = input.replace(/du/g, "dzu");
    input = input.replace(/ho/g, "ho");
    input = input.replace(/n([^aeiouy])/, "ŋ$1");
    input = input.replace(/n$/g, "ɴ");
    input = input.replace(/^ni/g, "ɲ");
    input = input.replace(/nny/g, "ɲ");

    input = input.replace(/f/g, "ɸ");
    input = input.replace(/g/g, "ɡ");
    input = input.replace(/h/g, "ç");
    input = input.replace(/j/g, "dʑ");
    input = input.replace(/ti/g, "tɕi");
    input = input.replace(/ts/g, "ts");
    input = input.replace(/y/g, "j");

    // /r/ is almost always [ɺ] in initial position
    input = input.replace(/^r/g, "ɺ");
    input = input.replace(/r/g, "ɾ");

    // Set up the basic IPA vowels.
    input = input.replace(/([ɕɸksth])i$/, "$1i̥");

    // the high vowels /i/ and /u/ become devoiced when between voiceless
    // consonants
    input = input.replace(
        /([ɕɸksth])i\.([ɕɸksth])/g,
        "$1i̥.$2");
    input = input.replace(/([ɕɸksth])u$/, "$1ɯ̥");
    input = input.replace(/u/g, "ɯ");

    // Return the results.
    return input;
}
