import { DictionaryTermData } from "./DictionaryTermData";

/**
 * The gender implementations of a given part of speech set.
 */
export interface DictionaryGenderData
{
    /** The masculine or male terms. */
    masculine?: DictionaryTermData[];

    /** The feminine or female terms. */
    feminine?: DictionaryTermData[];

    /** The neutral or neuter terms. */
    neuter?: DictionaryTermData[];
}
