import { DictionaryGenderData } from "./DictionaryGenderData";
import { DictionaryTermData } from "./DictionaryTermData";

/**
 * The parts of speech collection.
 */
export interface DictionaryPartsOfSpeechData
{
    /** The noun uses of the entry. */
    noun?: DictionaryGenderData;

    /** The verb sub-entries. */
    verb?: DictionaryGenderData;

    /** Adverbs. */
    adv?: DictionaryTermData[];

    /** Adjectives. */
    adj?: DictionaryTermData[];
}
