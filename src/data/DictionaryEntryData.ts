import { DictionaryPartsOfSpeechData } from "./DictionaryPartsOfSpeechData";

/**
 * A single dictionary entry modelled after Wikitionary.
 */
export interface DictionaryEntryData
{
    /**
     * The base name of the entry, without any inflection.
     */
    base: string;

    /**
     * Contains the parts of speech entries.
     */
    pos?: DictionaryPartsOfSpeechData;
}
