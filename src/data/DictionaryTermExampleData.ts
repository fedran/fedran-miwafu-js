export interface DictionaryTermExampleData
{
    /** The Miwafu text. */
    miw: string;

    /** English definition. */
    en: string;
}
