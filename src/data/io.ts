import * as fs from "fs";
import * as yaml from "js-yaml";
import { DictionaryEntryData } from "./DictionaryEntryData";

/**
 * Reads a dictionary YAML file and returns the entry.
 */
export function readDictionaryYamlSync(
    filename: string): DictionaryEntryData
{
    var buffer = fs.readFileSync(filename, "utf8");
    var entry: DictionaryEntryData = yaml.safeLoad(buffer);

    return entry;
}
