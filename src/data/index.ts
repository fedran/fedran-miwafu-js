export * from "./DictionaryEntryData";
export * from "./DictionaryGenderData";
export * from "./DictionaryPartsOfSpeechData";
export * from "./DictionaryTermData";
export * from "./DictionaryTermExampleData";
export * from "./io";
