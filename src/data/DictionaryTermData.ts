import { DictionaryTermExampleData } from "./DictionaryTermExampleData";

/**
 * A single term for a part of speech.
 */
export interface DictionaryTermData
{
    /** The definition of the term. */
    def: string;

    /** A list of lowercase tags for this entry. */
    tags?: string[];

    /** A list of examples for the term. */
    examples?: DictionaryTermExampleData[];
}
