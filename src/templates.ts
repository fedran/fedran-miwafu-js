import * as _ from "lodash";
import { DictionaryEntryData } from "./data";
import { inflectFeminine, inflectMasculine, inflectNeuter } from "./inflect";
import { ipa } from "./ipa";

export function createTemplateContext(
    entry: DictionaryEntryData,
    defines: string[])
    : any
{
    // The basic context has the full entry along with a stub for
    // pronouncation.
    let context: any = {
        entry,
        male: inflectMasculine(entry.base),
        neuter: inflectNeuter(entry.base),
        female: inflectFeminine(entry.base)
    };

    // Figure out the pronouncation and inject that.
    let pronounce: any = {};

    if (entry.pos)
    {
        const hasNounMale = entry.pos.noun && !!entry.pos.noun.masculine;
        const hasNounNeuter = entry.pos.noun && !!entry.pos.noun.neuter;
        const hasNounFemale = entry.pos.noun && !!entry.pos.noun.feminine;
        const hasVerbMale = entry.pos.verb && !!entry.pos.verb.masculine;
        const hasVerbNeuter = entry.pos.verb && !!entry.pos.verb.neuter;
        const hasVerbFemale = entry.pos.verb && !!entry.pos.verb.feminine;
        const hasAdjective = !!entry.pos.adj;
        const hasAdverb = !!entry.pos.adv;

        if (hasAdverb || hasAdjective)
        {
            pronounce.base = {
                word: entry.base,
                ipa: ipa(entry.base)
            };
        }

        if (hasNounMale || hasVerbMale)
        {
            pronounce.masculine = {
                word: context.male,
                ipa: ipa(context.male)
            };
        }

        if (hasNounNeuter || hasVerbNeuter)
        {
            pronounce.neuter = {
                word: context.neuter,
                ipa: ipa(context.neuter)
            };
        }

        if (hasNounFemale || hasVerbFemale)
        {
            pronounce.feminine = {
                word: context.female,
                ipa: ipa(context.female)
            };
        }
    }

    context.pronounce = pronounce;

    // Add in any parameters from the command-line.
    for (const def of defines || [])
    {
        const parts = def.split('=');

        if (parts.length !== 2)
        {
            process.stderr.write(
                "Cannot parse define '" + def + "', should be 'a=b'");
            continue;
        }

        _.set(context, parts[0], parts[1]);
    }

    // Return the resulting context.
    return context;
}
