/**
 * A list of all the base syllabls for the language.
 */
export var baseSyllables = [
    "a", "wa", "ra", "ma", "pa", "ba", "ha", "na", "da", "ta", "za", "sa", "ga",
    "ka", "fa", "e", "we", "re", "me", "pe", "be", "he", "ne", "de", "te", "ze",
    "se", "ge", "ke", "fe", "i", "wi", "ri", "mi", "pi", "bi", "hi", "ni",
    "chi", "ji", "shi", "gi", "ki", "fi", "o", "wo", "ro", "mo", "po", "bo",
    "ho", "no", "do", "to", "jo", "so", "go", "ko", "fo", "u", "wu", "ru", "mu",
    "pu", "bu", "hu", "nu", "tsu", "zu", "su", "gu", "ku", "fu", "rya", "mya",
    "pya", "bya", "hya", "nya", "chya", "jya", "shya", "gya", "kya", "ryo",
    "myo", "pyo", "byo", "hyo", "nyo", "chyo", "jyo", "shyo", "gyo", "kyo",
    "ryu", "myu", "pyu", "byu", "hyu", "nyu", "chyu", "jyu", "shyu", "gyu",
    "kyu", "n",
];

// Figure out the regular expressions from the above list.
const longestToShortestBaseSyllables = [...baseSyllables];

longestToShortestBaseSyllables
    .sort((a, b) => b.length - a.length || a.localeCompare(b));

const longestToShortestRegExpSyllables = longestToShortestBaseSyllables
    .map(a =>
    {
        return a
            .replace(/a$/, "[aáàā]")
            .replace(/e$/, "[eéèē]")
            .replace(/i$/, "[iíìī]")
            .replace(/o$/, "[oóòō]")
            .replace(/u$/, "[uúùū]");
    });

const syllablesRegExp =
    new RegExp(`(${longestToShortestRegExpSyllables.join("|")})`, "i");

/**
 * Splits a word into its syllables.
 */
export function splitSyllables(word: string): string[]
{
    return word.split(syllablesRegExp).filter((s) => !!s);
}
