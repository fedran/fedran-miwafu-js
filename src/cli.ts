#!/usr/bin/env node
import * as yargs from "yargs";

yargs
    .usage("$0 <cmd> [args]")
    .commandDir("tools")
    .demandCommand(1)
    .help("help")
    .argv;
