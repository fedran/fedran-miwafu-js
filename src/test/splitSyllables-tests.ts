import * as expect from "expect";
import "mocha";
import * as lib from "../index";
import { inflectMasculineSyllable, inflectNeuterSyllable, inflectFeminineSyllable } from "../inflect";
import { baseSyllables } from "../syllables";

describe("split syllables", function()
{
    it("miwāfu", function(done)
    {
        let input = "miwāfu";
        let output = ["mi", "wā", "fu"];

        expect(lib.splitSyllables(input)).toEqual(output);
        done();
    });

    it("kanéko", function(done)
    {
        let input = "kanéko";
        let output = ["ka", "né", "ko"];

        expect(lib.splitSyllables(input)).toEqual(output);
        done();
    });

    for (const syllable of baseSyllables)
    {
        it("baseSyllables: " + syllable, function(done)
        {
            var output = [
                syllable,
                inflectMasculineSyllable(syllable),
                inflectNeuterSyllable(syllable),
                inflectFeminineSyllable(syllable),
            ];
            var input = output.join("");

            expect(lib.splitSyllables(input)).toEqual(output);
            done();
        });

        if (!syllable.match(/^[aeiou]$/))
        {
            it("baseSyllables with n: " + syllable, function(done)
            {
                var output = [
                    "n",
                    syllable,
                    "n",
                    inflectMasculineSyllable(syllable),
                    "n",
                    inflectNeuterSyllable(syllable),
                    "n",
                    inflectFeminineSyllable(syllable),
                    "n",
                ];
                var input = output.join("");

                expect(lib.splitSyllables(input)).toEqual(output);
                done();
            });
        }
    }
});
