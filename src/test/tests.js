"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var expect = require("expect");
require("mocha");
var lib = require("../index");
describe("fedran-miwafu", function () {
    it("ipa for miwāfu", function (done) {
        var input = "miwāfu";
        var output = "mi.waː.ɸɯ̥";
        expect(lib.ipa(input)).toEqual(output);
        done();
    });
    it("ipa for kanéko", function (done) {
        var input = "kanéko";
        var output = "ka.ꜛne.ko";
        expect(lib.ipa(input)).toEqual(output);
        done();
    });
    it("ipa for rutejìmo", function (done) {
        var input = "rutejìmo";
        var output = "ɺɯ.te.ꜜdʑi.mo";
        expect(lib.ipa(input)).toEqual(output);
        done();
    });
    it("ipa for waryoni pagani héru", function (done) {
        var input = "waryoni pagani héru";
        var output = "wa.ɾjo.ni pa.ɡa.ni ꜛçe.ɾɯ";
        expect(lib.ipa(input)).toEqual(output);
        done();
    });
    it("ipa for oe shimusògo i fapòdi eyo rutejìmo", function (done) {
        var input = "oe shimusògo i fapòdi eyo rutejìmo";
        var output = "o.e ɕi.mɯ.ꜜso.ɡo i ɸa.ꜜpo.di e.jo ɺɯ.te.ꜜdʑi.mo";
        expect(lib.ipa(input)).toEqual(output);
        done();
    });
    it("ipa for shikoku", function (done) {
        var input = "shikoku";
        var output = "ɕi̥.ko.kɯ̥";
        expect(lib.ipa(input)).toEqual(output);
        done();
    });
});
//# sourceMappingURL=tests.js.map