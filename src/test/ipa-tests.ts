import * as expect from "expect";
import "mocha";
import * as lib from "../index";

describe("fedran-miwafu", function()
{
    it("ipa for miwāfu", function(done)
    {
        let input = "miwāfu";
        let output = "mi.waː.ɸɯ̥";

        expect(lib.ipa(input)).toEqual(output);
        done();
    });

    it("ipa for kanéko", function(done)
    {
        let input = "kanéko";
        let output = "ka.ꜛne.ko";

        expect(lib.ipa(input)).toEqual(output);
        done();
    });

    it("ipa for rutejìmo", function(done)
    {
        let input = "rutejìmo";
        let output = "ɺɯ.te.ꜜdʑi.mo";

        expect(lib.ipa(input)).toEqual(output);
        done();
    });

    it("ipa for waryoni pagani héru", function(done)
    {
        let input = "waryoni pagani héru";
        let output = "wa.ɾjo.ni pa.ɡa.ni ꜛçe.ɾɯ";

        expect(lib.ipa(input)).toEqual(output);
        done();
    });

    it("ipa for oe shimusògo i fapòdi eyo rutejìmo", function(done)
    {
        let input = "oe shimusògo i fapòdi eyo rutejìmo";
        let output = "o.e ɕi.mɯ.ꜜso.ɡo i ɸa.ꜜpo.di e.jo ɺɯ.te.ꜜdʑi.mo";

        expect(lib.ipa(input)).toEqual(output);
        done();
    });

    it("ipa for shikoku", function(done)
    {
        let input = "shikoku";
        let output = "ɕi̥.ko.kɯ̥";

        expect(lib.ipa(input)).toEqual(output);
        done();
    });
});
