import * as fs from "fs";
import * as Handlebars from "handlebars";
import * as HandlebarsHelpers from "handlebars-helpers";
import * as miwafu from "../../index";

// Register the helpers to give us more useful functions.
HandlebarsHelpers(Handlebars);

Handlebars.registerHelper("markdownPara", function(text, prefix)
{
    return text.trim().split("\n").join("\n\n" + prefix);
});

// The exports are all yargs-required.
export const command = "markdown <entry> <template>";
export const describe = "Renders a YAML dictionary file into Markdown.";

export function builder(yargs)
{
    return yargs
        .array("define");
};

export function handler(args: any): void
{
    // Load the entry from YAML.
    const entry = miwafu.readDictionaryYamlSync(args.entry);

    // Load the template and render it.
    const templateBuffer = fs.readFileSync(args.template);
    const template = Handlebars.compile(templateBuffer.toString());
    const context = miwafu.createTemplateContext(entry, args.define);
    const result = template(context);

    // Figure out how we'll write it out.
    process.stdout.write(result);
}
