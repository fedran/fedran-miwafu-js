export const command = "render <command>";
export const describe = "Renders dictionary YAML files.";

export function builder(yargs)
{
    return yargs
        .commandDir("render");
}

export function handler(argv: any): void
{
    // This block left intentionally empty.
}
