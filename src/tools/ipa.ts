import * as miwafu from "../index";

export const command = "ipa";
export const describe = "Displays the IPA for the given words.";

export function builder(yargs)
{
    return yargs
        .demandCommand(1);
}

export function handler(argv: any): void
{
    const ipa = miwafu.ipa(argv._.splice(1).join(" "));

    process.stdout.write(ipa + "\n");
}
