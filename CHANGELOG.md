# [0.2.0](https://gitlab.com/fedran/fedran-miwafu-js/compare/v0.1.8...v0.2.0) (2018-12-27)


### Features

* extracted the template context as a function ([2cac6df](https://gitlab.com/fedran/fedran-miwafu-js/commit/2cac6df))

## [0.1.8](https://gitlab.com/fedran/fedran-miwafu-js/compare/v0.1.7...v0.1.8) (2018-11-29)


### Bug Fixes

* updated paths of tools to new layout ([9e0c372](https://gitlab.com/fedran/fedran-miwafu-js/commit/9e0c372))

## [0.1.7](https://gitlab.com/fedran/fedran-miwafu-js/compare/v0.1.6...v0.1.7) (2018-11-29)


### Bug Fixes

* fixed breaking of syllables and added additional tests ([5e6e5e5](https://gitlab.com/fedran/fedran-miwafu-js/commit/5e6e5e5))
