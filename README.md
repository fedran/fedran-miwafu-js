fedran-miwafu
=============

> A library for parsing and manipulating Miwāfu, a constructed language in D. Moonfire's Fedran setting.

## Setup and usage

Install `fedran-miwafu` using `npm`:

```sh
npm i fedran-miwafu
```

In your `gulpfile.js`:

```js
var miwafu = require('fedran-miwafu');

console.log(miwafu.ipa('miwāfu'));
// mi.waː.ɸɯ̥
```
